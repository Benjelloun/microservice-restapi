package com.home.person.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@ApiModel(description = "All details about the Person. ")

public class Personne implements Serializable {
    @Id
    @GeneratedValue
    @ApiModelProperty(notes = "The database generated Person ID")
    private Long idPerson;
    @ApiModelProperty(notes = "The database generated Person Name")
    private String name;
    @ApiModelProperty(notes = "The database generated Person FamilyName")
    private String familyName;
    @ApiModelProperty(notes = "The database generated Person Adress")
    private String adress;
    @ApiModelProperty(notes = "The database generated Person Mail")
    private String mail;
    @ApiModelProperty(notes = "The database generated Person Phone Number")
    private String phone;
    public Personne() {
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public String getName() {
        return name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getAdress() {
        return adress;
    }

    public String getMail() {
        return mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
