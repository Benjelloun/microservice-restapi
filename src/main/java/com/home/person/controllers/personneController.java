package com.home.person.controllers;

import com.home.person.entities.Personne;
import com.home.person.repository.IPersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/v1/personne")

public class personneController {
    @Autowired
    private IPersonneRepository iPersonneRepository;

    @PostMapping(value = "/create")
    public Personne createPersonne(@RequestBody Personne personne){
        return iPersonneRepository.save(personne);
    }

    @PutMapping(value = "/update/{id}")
    public Personne updatePersonne(@PathVariable(value = "id") Long id, @RequestBody Personne personne){
        if(id !=null){
            Optional<Personne> p = iPersonneRepository.findById(id);
            if(p != null){
                personne.setIdPerson(id);
               return iPersonneRepository.save(personne);
            }
        }
        return null;
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deletePersonne(@PathVariable(value = "id") Long id){
        if (id != null){
            Optional<Personne> p = iPersonneRepository.findById(id);
            if(p != null){
                iPersonneRepository.deleteById(id);
            }
        }
    }
    @GetMapping(value = "/getAll")
    public List<Personne> AllPersonne(){
        return iPersonneRepository.findAll();
    }

    @GetMapping(value = "/getOnePersonne/{id}")
    public Personne getPersonne(@PathVariable(value = "id") Long id){
        Optional<Personne> personne = iPersonneRepository.findById(id);
        return personne.get();
    }

    @GetMapping(value = "/all/byName/{name}")
    public List<Personne> getByName(@PathVariable String name) {
    return iPersonneRepository.getByName(name);
    }
    }
