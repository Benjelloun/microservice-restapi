package com.home.person.repository;

import com.home.person.entities.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface IPersonneRepository extends JpaRepository<Personne, Long> {

    public List<Personne> getByName(String name);


}
